#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include <string>

#define NOCOLOR	"\033[0m"

class Animal
{

protected:
	Animal(std::string type);
	std::string	_type;

public:
	Animal(void);
	Animal(const Animal &animal);
	Animal	&operator=(const Animal &animal);
	virtual ~Animal(void);

	std::string	getType(void);
	void		makeSound(void);

};

#endif
