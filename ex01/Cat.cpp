#include <iostream>
#include "Cat.hpp"

#define COLOR	"\033[94m"

Cat::Cat(void) :	Animal("Cat"), _brain(new Brain())
{
	std::cout	<< COLOR
				<< "A cat is born.\n"
				<< NOCOLOR;
}

Cat::Cat(const Cat &Cat)
{
	*this = Cat;
}

Cat	&Cat::operator=(const Cat &cat)
{
	_brain = new Brain((const Brain&)(*cat._brain));

	std::cout	<< COLOR
				<< "A cat is born.\n"
				<< NOCOLOR;
	return (*this);
}

Cat::~Cat(void)
{
	std::cout	<< COLOR
				<< "The cat is gone.\n"
				<< NOCOLOR;
	delete this->_brain;
}

void	Cat::makeSound(void)
{
	std::cout	<< COLOR
				<< "The cat meows.\n"
				<< NOCOLOR;
}
