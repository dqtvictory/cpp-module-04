#include <iostream>
#include "Cat.hpp"
#include "Dog.hpp"

#define ZOO_SIZE	6

int	main(void)
{
	Animal	*zoo[ZOO_SIZE];

	std::cout	<< "\n===== CREATING ANIMALS =====\n";
	for (int i = 0; i < ZOO_SIZE / 2; i++)
	{
		std::cout << "===== " << i << " =====\n";
		zoo[i] = new Cat();
	}
	for (int i = ZOO_SIZE / 2; i < ZOO_SIZE; i++)
	{
		std::cout << "===== " << i << " =====\n";
		zoo[i] = new Dog();
	}

	std::cout	<< "\n===== KILLING ANIMALS =====\n";
	for (int i = 0; i < ZOO_SIZE; i++)
	{
		std::cout << "===== " << i << " =====\n";
		delete zoo[i];
	}

	std::cout	<< "\n===== TESTING SINGLE ANIMALS =====\n";

	const Animal	*j = new Dog();
	const Animal	*i = new Cat();

	delete j;
	delete i;
}