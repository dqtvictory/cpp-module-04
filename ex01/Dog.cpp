#include <iostream>
#include "Dog.hpp"

#define COLOR	"\033[92m"

Dog::Dog(void) :	Animal("Dog"), _brain(new Brain())
{
	std::cout	<< COLOR
				<< "A dog is born.\n"
				<< NOCOLOR;
}

Dog::Dog(const Dog &dog)
{
	*this = dog;
}

Dog	&Dog::operator=(const Dog &dog)
{
	_brain = new Brain((const Brain&)(*dog._brain));

	std::cout	<< COLOR
				<< "A dog is born.\n"
				<< NOCOLOR;
	return (*this);
}

Dog::~Dog(void)
{
	std::cout	<< COLOR
				<< "The dog is gone.\n"
				<< NOCOLOR;
	delete this->_brain;
}

void	Dog::makeSound(void)
{
	std::cout	<< COLOR
				<< "The dog barks.\n"
				<< NOCOLOR;
}
