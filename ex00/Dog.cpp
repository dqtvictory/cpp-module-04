#include <iostream>
#include "Dog.hpp"

#define COLOR	"\033[92m"

Dog::Dog(void) :	Animal("Dog")
{
	std::cout	<< COLOR
				<< "A " << _type << " is born.\n"
				<< NOCOLOR;
}

Dog::Dog(const Dog &dog)
{
	*this = dog;
}

Dog	&Dog::operator=(const Dog &dog)
{
	(void)dog;

	std::cout	<< COLOR
				<< "A " << _type << " is born.\n"
				<< NOCOLOR;
	return (*this);
}

Dog::~Dog(void)
{
	std::cout	<< COLOR
				<< "The " << _type << " is gone.\n"
				<< NOCOLOR;
}

void	Dog::makeSound(void)
{
	std::cout	<< COLOR
				<< "The " << _type << " barks.\n"
				<< NOCOLOR;
}
