#include <iostream>
#include "Cat.hpp"

#define COLOR	"\033[94m"

Cat::Cat(void) :	Animal("Cat")
{
	std::cout	<< COLOR
				<< "A " << _type << " is born.\n"
				<< NOCOLOR;
}

Cat::Cat(const Cat &Cat)
{
	*this = Cat;
}

Cat	&Cat::operator=(const Cat &cat)
{
	(void)cat;

	std::cout	<< COLOR
				<< "A " << _type << " is born.\n"
				<< NOCOLOR;
	return (*this);
}

Cat::~Cat(void)
{
	std::cout	<< COLOR
				<< "The " << _type << " is gone.\n"
				<< NOCOLOR;
}

void	Cat::makeSound(void)
{
	std::cout	<< COLOR
				<< "The " << _type << " meows.\n"
				<< NOCOLOR;
}
