#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include <string>

#define NOCOLOR	"\033[0m"

class Animal
{

protected:
	Animal(std::string type);
	std::string	_type;

public:
	Animal(void);
	Animal(const Animal &animal);
	Animal	&operator=(const Animal &animal);
	~Animal(void);

	std::string	getType(void);
	void		makeSound(void);

};


class WrongAnimal
{

protected:
	WrongAnimal(std::string type);
	std::string	_type;

public:
	WrongAnimal(void);
	WrongAnimal(const WrongAnimal &animal);
	WrongAnimal	&operator=(const WrongAnimal &animal);
	~WrongAnimal(void);

	std::string	getType(void);
	void		makeSound(void);

};

#endif
