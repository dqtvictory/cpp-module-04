#ifndef CAT_HPP
#define CAT_HPP

#include "Animal.hpp"

class Cat : public Animal
{

public:

	Cat(void);
	Cat(const Cat &cat);
	Cat	&operator=(const Cat &cat);
	~Cat(void);

	virtual void	makeSound(void);

};


class WrongCat : public WrongAnimal
{

public:

	WrongCat(void);
	WrongCat(const WrongCat &cat);
	WrongCat	&operator=(const WrongCat &cat);
	~WrongCat(void);

};

#endif
