#include <iostream>
#include "Cat.hpp"

#define COLOR	"\033[96m"

WrongCat::WrongCat(void) :	WrongAnimal("WrongCat")
{
	std::cout	<< COLOR
				<< "A " << _type << " is born.\n"
				<< NOCOLOR;
}

WrongCat::WrongCat(const WrongCat &cat)
{
	*this = cat;
}

WrongCat	&WrongCat::operator=(const WrongCat &cat)
{
	(void)cat;

	std::cout	<< COLOR
				<< "A " << _type << " is born.\n"
				<< NOCOLOR;
	return (*this);
}

WrongCat::~WrongCat(void)
{
	std::cout	<< COLOR
				<< "The " << _type << " is gone.\n"
				<< NOCOLOR;
}
