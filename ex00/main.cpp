#include <iostream>
#include "Cat.hpp"
#include "Dog.hpp"

int	main(void)
{
	{
		std::cout	<< "\n===== CORRECT ANIMAL TEST =====\n";

		Animal	meta;
		Cat		cat;
		Dog		dog;

		std::cout	<< cat.getType() << '\n';
		std::cout	<< dog.getType() << '\n';

		cat.makeSound();
		dog.makeSound();
		meta.makeSound();
	}

	{
		std::cout	<< "\n===== WRONG ANIMAL TEST =====\n";

		WrongAnimal	wa;
		WrongCat	wc;
		
		std::cout	<< wa.getType() << '\n';
		std::cout	<< wc.getType() << '\n';

		wa.makeSound();
		wc.makeSound();
	}
}