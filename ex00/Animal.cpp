#include <iostream>
#include "Animal.hpp"

#define COLOR	"\033[95m"

Animal::Animal(std::string type) : _type(type)
{
	std::cout	<< COLOR
				<< "Some kind of animal is created.\n"
				<< NOCOLOR;
}

Animal::Animal(void) :	_type("Animal")
{
	std::cout	<< COLOR
				<< "Some kind of animal is created.\n"
				<< NOCOLOR;
}

Animal::Animal(const Animal &animal)
{
	*this = animal;
}

Animal	&Animal::operator=(const Animal &animal)
{
	_type = animal._type;
	std::cout	<< COLOR
				<< "Some kind of animal is created.\n"
				<< NOCOLOR;
	return (*this);
}

Animal::~Animal(void)
{
	std::cout	<< COLOR
				<< "The animal is gone.\n"
				<< NOCOLOR;
}

std::string	Animal::getType(void)
{
	return (_type);
}

void	Animal::makeSound(void)
{
	std::cout	<< COLOR
				<< "The animal just made a sound.\n"
				<< NOCOLOR;
}
