#include <iostream>
#include "Animal.hpp"

#define COLOR	"\033[91m"

WrongAnimal::WrongAnimal(std::string type) : _type(type)
{
	std::cout	<< COLOR
				<< "Some kind of wrong animal is created.\n"
				<< NOCOLOR;
}

WrongAnimal::WrongAnimal(void) :	_type("WrongAnimal")
{
	std::cout	<< COLOR
				<< "Some kind of wrong animal is created.\n"
				<< NOCOLOR;
}

WrongAnimal::WrongAnimal(const WrongAnimal &WrongAnimal)
{
	*this = WrongAnimal;
}

WrongAnimal	&WrongAnimal::operator=(const WrongAnimal &WrongAnimal)
{
	_type = WrongAnimal._type;
	std::cout	<< COLOR
				<< "Some kind of wrong animal is created.\n"
				<< NOCOLOR;
	return (*this);
}

WrongAnimal::~WrongAnimal(void)
{
	std::cout	<< COLOR
				<< "The wrong animal is gone.\n"
				<< NOCOLOR;
}

std::string	WrongAnimal::getType(void)
{
	return (_type);
}

void	WrongAnimal::makeSound(void)
{
	std::cout	<< COLOR
				<< "The wrong animal just made a sound.\n"
				<< NOCOLOR;
}
