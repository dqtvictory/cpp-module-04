#ifndef BRAIN_HPP
#define BRAIN_HPP

#include <string>

#define BRAIN_SIZE	100

class Brain
{

protected:
	std::string	_ideas[BRAIN_SIZE];

public:
	Brain(void);
	Brain(const Brain &brain);
	Brain	&operator=(const Brain &brain);
	~Brain(void);

};

#endif
