#include <iostream>
#include "Brain.hpp"

Brain::Brain(void)
{
	std::cout	<< "A brain is created\n";
}

Brain::Brain(const Brain &brain)
{
	*this = brain;
}

Brain	&Brain::operator=(const Brain &brain)
{
	for (int i = 0; i < BRAIN_SIZE; i++)
		_ideas[i] = brain._ideas[i];

	std::cout	<< "A brain is created\n";
	return (*this);
}

Brain::~Brain(void)
{
	std::cout	<< "A brain is deleted\n";
}