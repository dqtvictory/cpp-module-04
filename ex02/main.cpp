#include <iostream>
#include "Cat.hpp"
#include "Dog.hpp"

#define ZOO_SIZE	6

int	main(void)
{
	{
		Cat	cat;
		cat.makeSound();
		std::cout << "\nAnimal type: " << cat.getType() << "\n";
	}

	std::cout << "\n==========\n";

	{
		Dog dog;
		dog.makeSound();
		std::cout << "\nAnimal type: " << dog.getType() << "\n\n";
	}

	// Uncomment code below to get a compilation error
	
	// Animal	anim;
	// std::cout << "Animal type: " << anim.getType() << "\n";
}