#ifndef AMATERIA_HPP
#define AMATERIA_HPP

#include <string>
#include "Character.hpp"

class AMateria
{

protected:
	const std::string	_type;

public:
	AMateria(std::string const &type);
	AMateria(const AMateria &am);
	AMateria	&operator=(const AMateria &am);
	virtual ~AMateria(void);

	std::string const	&getType(void) const;
	virtual AMateria	*clone() const = 0;
	virtual void		use(ICharacter &target);

};

#endif
