#include "AMateria.hpp"

AMateria::AMateria(std::string const &type) : _type(type)
{}

AMateria::AMateria(const AMateria &am) : _type(am._type)
{}

AMateria	&AMateria::operator=(const AMateria &am)
{
	return ((AMateria&)am);
}

std::string const	&AMateria::getType(void) const
{
	return (_type);
}

AMateria::~AMateria(void) {}

void	AMateria::use(ICharacter &target)
{
	(void)target;
}
