#include <iostream>
#include "Ice.hpp"

Ice::Ice() : AMateria("ice")
{
	// std::cout << "A new ice is created\n";
}

Ice::Ice(const Ice &ice) : AMateria(ice._type)
{
	// (void)ice;
	// std::cout << "A new ice is created\n";
}

Ice	&Ice::operator=(const Ice &ice)
{
	return ((Ice&)ice);
}

Ice::~Ice(void)
{
	// std::cout << "An ice is destroyed\n";
}

Ice	*Ice::clone(void) const
{
	// std::cout << "An ice is cloned\n";
	return (new Ice(*this));
}

void	Ice::use(ICharacter &target)
{
	std::cout << "* shoots an ice bolt at " << target.getName() << " *\n";
}
