#include <iostream>
#include "Ice.hpp"
#include "Cure.hpp"
#include "Character.hpp"
#include "MateriaSource.hpp"

int	main()
{
	// Create a Materia Source and make it learn new materias
	IMateriaSource* src = new MateriaSource();
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());

	// Create a character and equip it with materias created by the Source
	ICharacter* me = new Character("me");
	me->equip(src->createMateria("cure"));	std::cout << me->getName() << " equipped cure\n";
	me->equip(src->createMateria("ice"));	std::cout << me->getName() << " equipped ice\n";
	
	// Display character's inventory
	std::cout << "Inventory of " << me->getName() << ": ";
	static_cast<Character *>(me)->listInventory();

	// Create new materias from the Source then equip the character
	AMateria	*i1 = src->createMateria("ice"),
				*i2 = src->createMateria("ice"),
				*c = src->createMateria("cure");
	me->equip(i1);	std::cout << me->getName() << " equipped " << i1->getType() << '\n';
	me->equip(c);	std::cout << me->getName() << " equipped " << c->getType() << '\n';
	me->equip(i2); // This line does not change anything since character is fully equipped

	// Display character's inventory again
	std::cout << "Inventory of " << me->getName() << ": ";
	static_cast<Character *>(me)->listInventory();

	Character	bob("bob");

	me->use(0, bob);	// use cure
	me->use(1, bob);	// use ice
	me->use(2, bob);	// use ice
	me->use(3, bob);	// use cure
	me->use(100, bob);	// doesn't do anything

	me->unequip(2);	std::cout << me->getName() << " unequipped something\n";
	me->unequip(3);	std::cout << me->getName() << " unequipped something\n";

	// Display character's inventory again
	std::cout << "Inventory of " << me->getName() << ": ";
	static_cast<Character *>(me)->listInventory();

	delete me;
	delete src;
	delete i1;
	delete i2;
	delete c;
}