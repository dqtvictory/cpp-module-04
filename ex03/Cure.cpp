#include <iostream>
#include "Cure.hpp"

Cure::Cure() : AMateria("cure")
{
	// std::cout << "A new cure is created\n";
}

Cure::Cure(const Cure &cure) : AMateria(cure._type)
{
	// (void)cure;
	// std::cout << "A new cure is created\n";
}

Cure	&Cure::operator=(const Cure &cure)
{
	return ((Cure &)cure);
}

Cure::~Cure(void)
{
	// std::cout << "A cure is destroyed\n";
}

Cure	*Cure::clone(void) const
{
	// std::cout << "A cure is cloned\n";
	return (new Cure(*this));
}

void	Cure::use(ICharacter &target)
{
	std::cout << "* heals " << target.getName() << "'s wounds *\n";
}
