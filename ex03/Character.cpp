#include <iostream>
#include <cstring>
#include "Character.hpp"
#include "AMateria.hpp"

Character::Character(std::string const &name) :	_name(name)
{
	// std::cout << "Character " << _name << " is born\n";

	// Set inventory to NULL (empty inventory)
	for (int i = 0; i < INV_SIZE; i++)
		_inventory[i] = NULL;
}

Character::Character(const Character &ch)
{
	*this = ch;
}

Character	&Character::operator=(const Character &ch)
{
	_name = ch._name;
	// std::cout << "Character " << _name << " is cloned\n";

	// Deep copy of inventory
	for (int i = 0; i < INV_SIZE; i++)
		if (ch._inventory[i])
			_inventory[i] = ch._inventory[i]->clone();
		else
			_inventory[i] = NULL;

	return (*this);
}

Character::~Character(void)
{
	// Delete inventory upon destruction
	for (int i = 0; i < INV_SIZE; i++)
		if (_inventory[i])
			delete _inventory[i];
	// std::cout << "Character " << _name << " is destroyed\n";
}

std::string const	&Character::getName(void) const
{
	return (_name);
}

void	Character::equip(AMateria *m)
{
	if (m == NULL)
		return ;

	for (int i = 0; i < INV_SIZE; i++)
		if (_inventory[i] == NULL)
		{
			// std::cout << _name << " equips " << m->getType() << '\n';
			_inventory[i] = m;
			return ;
		}
}

void	Character::unequip(int idx)
{
	// If invalid index or item at idx doesn't exist, do nothing
	if (idx < 0 || idx >= INV_SIZE || _inventory[idx] == NULL)
		return ;

	// std::cout << _name << " unequips " << _inventory[idx]->getType() << '\n';
	_inventory[idx] = NULL;
}

void	Character::use(int idx, ICharacter &target)
{
	// If invalid index or item at idx doesn't exist, do nothing
	if (idx < 0 || idx >= INV_SIZE || _inventory[idx] == NULL)
		return ;
	std::cout << _name << ' ';
	_inventory[idx]->use(target);
}

void	Character::listInventory(void)
{
	for (int i = 0; i < INV_SIZE; i++)
		if (_inventory[i])
			std::cout << _inventory[i]->getType() << ' ';
	std::cout << '\n';
}
