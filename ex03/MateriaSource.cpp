#include <iostream>
#include "MateriaSource.hpp"

MateriaSource::MateriaSource(void)
{
	for (int i = 0; i < MEM_SIZE; i++)
		_memory[i] = NULL;
}

MateriaSource::MateriaSource(const AMateria &am)
{
	*this = am;
}

MateriaSource	&MateriaSource::operator=(const MateriaSource &am)
{
	for (int i = 0; i < MEM_SIZE; i++)
		if (am._memory[i])
			_memory[i] = am._memory[i]->clone();
		else
			_memory[i] = NULL;
	return (*this);
}

MateriaSource::~MateriaSource(void)
{
	for (int i = 0; i < MEM_SIZE; i++)
		if (_memory[i])
			delete _memory[i];
}

void		MateriaSource::learnMateria(AMateria *m)
{
	for (int i = 0; i < MEM_SIZE; i++)
		if (_memory[i] == NULL)
		{
			_memory[i] = m;
			return ;
		}
}

AMateria	*MateriaSource::createMateria(std::string const &type)
{
	for (int i = 0; i < MEM_SIZE; i++)
		if (_memory[i] && _memory[i]->getType() == type)
			return (_memory[i]->clone());
	return (NULL);
}
