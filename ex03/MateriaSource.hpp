#ifndef MATERIASOURCE_HPP
#define MATERIASOURCE_HPP

#include "AMateria.hpp"

#define MEM_SIZE	4

class IMateriaSource
{

public:
	virtual ~IMateriaSource() {}
	virtual void		learnMateria(AMateria *m) = 0;
	virtual AMateria	*createMateria(std::string const &type) = 0;

};


class MateriaSource : public IMateriaSource
{

private:
	AMateria	*_memory[MEM_SIZE];

public:
	MateriaSource(void);
	MateriaSource(const AMateria &am);
	MateriaSource	&operator=(const MateriaSource &am);
	~MateriaSource(void);

	void		learnMateria(AMateria *m);
	AMateria	*createMateria(std::string const &type);

};

#endif
